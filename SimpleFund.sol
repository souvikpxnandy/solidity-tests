
// SPDX-License-Identifier: MIT

// CrowdFund 
// Smart contract that lets anyone deposit ETH into the contract
// Owner can set minimun worth USD to be accepted
// Owner can withdraw , afetr withdrawal reset conctrat 

// deployed on rinkby testnet : 0x43F251b16d281df76837653AEB40e55E3281413A
pragma solidity ^0.8.0;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


contract SimpleFund {
     
    //mapping to store which address depositeded how much ETH
    mapping(address => uint256) public addressToAmountFunded;
    // array of addresses who deposited
    address[] public funders;
    //address of the owner (who deployed the contract)
    address public owner;

    uint256 public collectedBalance;
    uint256 public minDepositUSD = 50;

    // AggregatorV3Interface address for rinkby
    address AggV3Address = 0x8A753747A1Fa494EC906cE90E9f37563A8AF630e;
    
    // the first person to deploy the contract is
    // the owner
    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner{
        //is the message sender owner of the contract?
        require(msg.sender == owner, "You don't have permission for this action");
        _ ;
    }

    function getPrice() public view returns(uint256){
        AggregatorV3Interface priceFeed = AggregatorV3Interface(AggV3Address);
        (,int price,,,) = priceFeed.latestRoundData();
        //  ETH to USD - returns in 8 decimal point, converting it to 18 decimal points as wei
         return uint256(price * 10**10);
    }

    function getConversionRate(uint256 ethAmount) public view returns(uint256){
        uint256 ethPrice = getPrice();
        uint256 ethAmountInUsd = (ethPrice * ethAmount) / 10**18;
        // the actual ETH/USD conversation rate, after adjusting the extra 0s.
        return ethAmountInUsd;
    }

    modifier checkMinDeposit(){
        // 18 digit number to be compared with donated amount 
        uint256 minimumUSD = minDepositUSD * 10 ** 18;
        //is the donated amount less than minDepositUSD ( initially 50) USD revert
        string memory _msg = string( abi.encodePacked("You need to spend ETH atleast worth of $", Strings.toString(minDepositUSD)) );
        require(getConversionRate(msg.value) >= minimumUSD, _msg);
        _ ;
    }

    function fund() public checkMinDeposit payable {
    	//if not, add to mapping and funders array
        addressToAmountFunded[msg.sender] += msg.value;
        funders.push(msg.sender);
        collectedBalance += msg.value;
    }


    function withdraw() public onlyOwner payable {
        // transfer all balance to caller
        payable(msg.sender).transfer(address(this).balance);
        collectedBalance = 0;
        //iterate through all the mappings and make them 0
        //since all the deposited amount has been withdrawn
        for (uint256 funderIndex=0; funderIndex < funders.length; funderIndex++){
            address funder = funders[funderIndex];
            addressToAmountFunded[funder] = 0;
        }
        //funders array will be initialized to 0
        funders = new address[](0);

    }

    function updateMinDepositUSD(uint256 _newUSDVal) public onlyOwner {
        minDepositUSD = _newUSDVal;
    }


}
